var inicio = new Vue({
	
	el:"#app",
	data:{
		mensagem:'',
		listaFornecedor: [],
        listaFornecedorHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "id", label:"id"}
		],
		produto:{
			fabricante:{
				id:0,
				nome:''
			}
		},
		sucesso:'none',
		query: location.search.slice(1).split('='),
		id:0,
		titulo:'Cadastro de produto'
	},
    created: function(){
        let vm =  this;
        vm.buscaFornecedor();
        if(this.query[1]>0){
        	this.titulo='Alterar produto'
        	this.buscarProduto();
        }
    },
    methods:{
        buscaFornecedor: function(){
			const vm = this;
			axios.get("/mercado/rs/fabicantes")
			.then(response => {vm.listaFornecedor = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
		buscarProduto: function(){
			const vm = this;
			axios.get("/mercado/rs/produtos/"+this.query[1])
			.then(response => {
				vm.produto = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
		salvar:function(){
			if(this.query[1]>0){
				axios.put("/mercado/rs/produtos/"+this.query[1],this.produto).then(resposta => {
					this.mensagem = 'Item alterado';
				});
				document.getElementById('sucesso').style.display = 'block';
			}else{
			axios.post("/mercado/rs/produtos",this.produto).then(resposta => {
				this.mensagem = resposta;
				document.getElementById('sucesso').style.display = 'block';
				this.mensagem = 'Item incluido';
			});
		}
    }
    }
	
});