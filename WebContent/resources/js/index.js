var inicio = new Vue({
	el:"#inicio",
    data: {
        listaProdutos: [],
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		],
		mensagem:''
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
    },
    methods:{
        buscaProdutos: function(){
			const vm = this;
			axios.get("/mercado/rs/produtos")
			.then(response => {vm.listaProdutos = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		excluir: function (id) {
			axios.delete("/mercado/rs/produtos/"+id).then(
					resposta => {
						this.produto = this.buscaProdutos();
						this.mensagem = 'Item excluido com sucesso!';
						document.getElementById('sucesso').style.display = 'block';
					}
			).catch(e => {
				document.getElementById('erro').style.display = 'block';
			});
	    },
	    alterar: function(id) {
	    	window.location.href = "/mercado/novo-produto.html?id="+id;
		}
    }
});